# Youtube Downloader

This script allows you to download videos with the best resolution and sound quality from [YouTube](https://www.youtube.com/).

## Local installing
1. Create a directory and go to it. Clone project into this folder:

    ```
    $ mkdir project_folder
    $ cd project_folder
    $ git clone https://gitlab.com/aleksei-g/yotube_downloader.git .
    ```
    
2. Create a virtual environment with Python 3.6 and install all required 
packeges:

    ```
    $ python3.6 -m venv virtualenv_path
    $ source virtualenv_path/bin/activate
    (virtualenv_path) $ pip install -r requirements.txt
    ```
3. Install ffmpeg:
    ```
    sudo apt update
    sudo apt install ffmpeg
    ```
    
## Run script
Make sure that you are in the project folder and have a virtual environment activated.
To run script, use one of the following command:
```
(virtualenv_path) $ python3 script.py
```
and then pass URL (or comma-separated list of URLs)

or pass URL in parameter `-u` (`--urls`):
```
(virtualenv_path) $ python3 script.py -u url1,url2
```
The script will save the video files in the current directory.