import argparse
import youtube_dl


YDL_OPTS_DEFAULT = {
    'format': 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best',
}


def create_parser():
    parser = argparse.ArgumentParser(description='Download video from YouTube.')
    parser.add_argument('-u', '--urls', help='Pass list of urls from YouTube '
                                             'separated ",".')
    return parser


def download(list_of_urls, ydl_list=YDL_OPTS_DEFAULT):
    ydl = youtube_dl.YoutubeDL(ydl_list)
    ydl.download(list_of_urls)


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args()
    if namespace.urls:
        urls_str = namespace.urls
    else:
        urls_str = input('Enter list of urls from YouTube (separated ","): ')
    if urls_str == '':
        print('Urls is empty!\nExit.')
        exit(0)
    urls = urls_str.split(sep=',')
    download(urls)
